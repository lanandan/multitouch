package com.example.multitouch;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity {
	Button b1;
	TextView tv1;
	RadioGroup rg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		b1 = (Button) findViewById(R.id.button1);
		tv1 = (TextView) findViewById(R.id.textView1);
		rg = (RadioGroup) findViewById(R.id.radioGroup1);
		b1.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				drag(event, v);
				return true;
			}

			private void drag(MotionEvent event, View v) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) v
						.getLayoutParams();
				switch (event.getAction()) {
				case MotionEvent.ACTION_MOVE: {
					params.topMargin = (int) event.getRawY() - (v.getHeight());
					params.leftMargin = (int) event.getRawX()
							- (v.getWidth() / 2);
					v.setLayoutParams(params);
					break;
				}
				case MotionEvent.ACTION_UP: {
					params.topMargin = (int) event.getRawY() - (v.getHeight());
					params.leftMargin = (int) event.getRawX()
							- (v.getWidth() / 2);
					v.setLayoutParams(params);
					break;
				}
				case MotionEvent.ACTION_DOWN: {
					v.setLayoutParams(params);
					break;
				}
				}
			}
		});
		tv1.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				drag(event, v);
				return true;
			}

			private void drag(MotionEvent event, View v) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) v
						.getLayoutParams();
				switch (event.getAction()) {
				case MotionEvent.ACTION_MOVE: {
					params.topMargin = (int) event.getRawY() - (v.getHeight());
					params.leftMargin = (int) event.getRawX()
							- (v.getWidth() / 2);
					v.setLayoutParams(params);
					break;
				}
				case MotionEvent.ACTION_UP: {
					params.topMargin = (int) event.getRawY() - (v.getHeight());
					params.leftMargin = (int) event.getRawX()
							- (v.getWidth() / 2);
					v.setLayoutParams(params);
					break;
				}
				case MotionEvent.ACTION_DOWN: {
					v.setLayoutParams(params);
					break;
				}
				}
			}
		});
		rg.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				drag(event, v);
				return true;
			}

			private void drag(MotionEvent event, View v) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) v
						.getLayoutParams();
				switch (event.getAction()) {
				case MotionEvent.ACTION_MOVE: {
					params.topMargin = (int) event.getRawY() - (v.getHeight());
					params.leftMargin = (int) event.getRawX()
							- (v.getWidth() / 2);
					v.setLayoutParams(params);
					break;
				}
				case MotionEvent.ACTION_UP: {
					params.topMargin = (int) event.getRawY() - (v.getHeight());
					params.leftMargin = (int) event.getRawX()
							- (v.getWidth() / 2);
					v.setLayoutParams(params);
					break;
				}
				case MotionEvent.ACTION_DOWN: {
					v.setLayoutParams(params);
					break;
				}
				}
			}
		});
	}

}
